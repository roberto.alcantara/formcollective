<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Nota extends Eloquent{
    //
    protected $collection='notas' ;
    protected $fillable =["nombre","dexcripcion"];
}
