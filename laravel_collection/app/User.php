<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
class User extends \Jenssegers\Mongodb\Eloquent\Model implements
AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name', 'email', 'password',
    ];

   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
//    protected $fillable = [
//        'name', 'email', 'password','phone','photo','first_name', 'contact_name', 'address', 'phone_number', 'fiscal_number', 'about_us'
//     ];

   /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
   protected $hidden = [
       'password', 'remember_token',
   ];

   public function subscription()
   {
       return $this->hasMany('App\Models\EmployerSubscription','employer_id','_id');
   }
   public function jobseekers()
   {
       return $this->hasOne('App\Models\JobSeekers');
   }


   public function experience()
   {
       return $this->hasMany('App\Models\Experience')->where('status',true);
   }
}